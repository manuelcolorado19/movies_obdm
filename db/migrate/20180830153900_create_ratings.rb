class CreateRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :ratings do |t|
      t.string :source
      t.string :value
      t.references :movie, foreign_key: true
    end
  end
end
