class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :year
      t.string :directors
      t.string :actor
      t.string :genre
      t.string :poster
    end
  end
end
