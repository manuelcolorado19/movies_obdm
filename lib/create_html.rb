module Show_movie

  def create_index get_all
      File.open("index.html", "w") do |f|
        f.write("<html>\n")
        f.write("<head>\n<title>OMDBAPI</title>\n</head>\n")
        f.write('<link rel="stylesheet" type="text/css" media="screen" href="style.css"')
        f.write("<body>\n")
        f.write("<section>\n")
        get_all.each do |one| 
          f.write('<div class="movie">')
          f.write("<aside>\n")
          f.write("<img src='#{one.poster}'>") 
          f.write("<p>Titulo:#{one.title}<p>\n")
          f.write("<p>Publicacion:#{one.year}<p>\n")
          f.write("<p>Director(s):#{one.directors}<p>\n")
          f.write("<p>Actor(s):#{one.actor}<p>\n")
          f.write("<p>Genero:#{one.genre}<p>\n")
          f.write("</aside>\n")         
          f.write("</div>\n")
        end
        f.write("</section>\n")
        f.write("</body>\n</html>\n")
      end  
  end
end


# <meta charset="utf-8" />
# <meta http-equiv="X-UA-Compatible" content="IE=edge">
# <title>Poyecto Css</title>
# <meta name="viewport" content="width=device-width, initial-scale=1">
# <link rel="stylesheet" type="text/css" media="screen" href="css/stylo.css" />
